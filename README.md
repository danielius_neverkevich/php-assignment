ads
===

 https://www.dropbox.com/s/biyhsfelw5cd9ls/php-assignment.md?dl=0
 
 http://assignment.daniel-dev.com/

 Download the source :
 
 ```sh
   git clone https://bitbucket.org/danielius_neverkevich/php-assignment.
 ```
 
 ```sh
    cd php-assignment
```
 
 Install the dependencies and configure web:
 
 ```sh
 composer install
  ```
  
 To create a database and tables:
 
 ```sh
 php app/console doctrine:database:create
 php app/console doctrine:schema:create          
   ```
   
  Install assets:
  
  ```sh
  php app/console assets:install  
   ```
