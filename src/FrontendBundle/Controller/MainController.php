<?php

namespace FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use FrontendBundle\Entity\Ad;
use FrontendBundle\Entity\User;
use Symfony\Component\Security\Acl\Exception\Exception;

class MainController extends Controller
{
    /**
     * @Route("/", name="index")
     * @Template("FrontendBundle:Main:index.html.twig")
     */
    public function indexAction(Request $request)
    {
       // $ads = $this->getDoctrine()->getRepository(Ad::class )->findAll();

        $ads = $this->getDoctrine()->getRepository(Ad::class )->filter($request, array());

        return array('ads' => $ads);
    }

    /**
     * @Route("/ad-view/{id}", name="ad_view")
     * @Template("FrontendBundle:Main:ad_view.html.twig")
     */
    public function adViewAction($id)
    {
        $ad = $this->getDoctrine()->getRepository(Ad::class )->findOneById($id);

        if (!$ad) {
            throw $this->createNotFoundException('The ad does not exist');
        }

        $user = $ad->getUser();

        $breadcrumbs = array(
            array('name' => $user->getUsername(), 'url' => $this->generateUrl('user_ad_view', array('id' => $user->getId()))),
            array('name' => $ad->getTitle())
        );

        return array('ad' => $ad, 'breadcrumbs' => $breadcrumbs);
    }

    /**
     * @Route("/user-ad-view/{id}", name="user_ad_view")
     * @Template("FrontendBundle:Main:index.html.twig")
     */
    public function userAdView($id, Request $request)
    {
        $user = $this->getDoctrine()->getRepository(User::class )->findOneById($id);

        if (!$user) {
            throw $this->createNotFoundException('The user does not exist');
        }

        $ads = $this->getDoctrine()->getRepository(Ad::class )->filter($request, array(
            'user_id' => $user->getId()
        ));

        $breadcrumbs = array(
            array('name' => $user->getUsername())
        );

        return array('ads' => $ads, 'breadcrumbs' => $breadcrumbs);
    }
}

