<?php

namespace FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use FrontendBundle\Entity\Ad;
use FrontendBundle\Form\AdType;
use FrontendBundle\Entity\User;

class Dashboard extends Controller
{
    /**
     * @Route("/dashboard", name="dashboard_main")
     * @Template("FrontendBundle:Main:index.html.twig")
     */
    public function mainAction(Request $request)
    {
        $user = $this->getUser();

        $ads = $this->getDoctrine()->getRepository(Ad::class )->filter($request, array(
            'user_id' => $user->getId()
        ));

        $breadcrumbs = array(
            array('name' => 'My Ads')
        );

        return array('ads' => $ads, 'breadcrumbs' => $breadcrumbs);
    }

    /**
     * @Route("/dashboard/new-ad", name="dashboard_new_ad")
     * @Template("FrontendBundle:Dashboard:add_ad.html.twig")
     */
    public function newAdAction(Request $request)
    {
        $ad = new Ad();

        $form = $this->createForm(AdType::class, $ad);
        $form->add('save', 'submit', array(
            'label' => 'Create',
            'attr' => array(
                'class' => 'btn btn-primary'
            )
        ));

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em = $this->getDoctrine()->getManager();
            $ad->setCreateDate(new \DateTime());
            $ad->setUser($this->getUser());

            $em->persist($ad);
            $em->flush();

            return $this->redirectToRoute('dashboard_main');
        }

        $breadcrumbs = array(
            array('name' => 'New Ad')
        );

        return array('form' => $form->createView(), 'breadcrumbs' => $breadcrumbs);
    }
}
