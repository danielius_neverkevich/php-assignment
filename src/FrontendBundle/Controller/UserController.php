<?php

namespace FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use FrontendBundle\Entity\User;
use FrontendBundle\Form\UserType;

class UserController extends Controller
{
    /**
     * @Route("/create-user", name="new_user")
     * @Template("FrontendBundle:User:newUser.html.twig")
     */
    public function newUserAction(Request $request)
    {
        $user = new User();
        $errors = array();

        $form = $this->createForm(UserType::class, $user);
        $form->add('save', 'submit', array(
            'label' => 'Create',
            'attr' => array(
                'class' => 'btn btn-primary'
            )
        ));

        $form->handleRequest($request);

        if ($user->getEmail()) {

            $userRep = $this->getDoctrine()->getRepository(User::class);
            $isDuplicate = $userRep->findOneByEmail($user->getEmail());

            if ($isDuplicate) {
                $errors[] = 'Email is used';
            }
        }

        if ($form->isSubmitted() && $form->isValid() && empty($errors)) {

            $password = $this->get('security.password_encoder')->encodePassword($user, $user->getPlainPassword());
            $user->setPassword($password);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('login');
        }

        $breadcrumbs = array(
            array('name' => 'New User Registration')
        );

         return array('form' => $form->createView(), 'errors' => $errors, 'breadcrumbs' => $breadcrumbs);
    }

    /**
     * @Route("/login", name="login")
     * @Template("FrontendBundle:User:LogIn.html.twig")
     */
    public function LogInAction(Request $request)
    {
        $authenticationUtils = $this->get('security.authentication_utils');
        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        $breadcrumbs = array(
            array('name' => 'New User Registration')
        );

        return array('last_username' => $lastUsername, 'error'=> $error, 'breadcrumbs' => $breadcrumbs);
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout()
    {
        $this->get('security.token_storage')->setToken(null);
        $this->get('request')->getSession()->invalidate();

        return $this->redirectToRoute('index');
    }
}
