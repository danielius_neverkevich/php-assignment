<?php

namespace FrontendBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MainControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/index');
    }

    public function testAds()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/ads');
    }

    public function testAdview()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/adView');
    }

}
